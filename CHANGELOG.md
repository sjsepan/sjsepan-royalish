# Royalish Color Theme - Change Log

## [0.2.2]

- tweak widget/notification borders and backgrounds

## [0.2.1]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.2.0]

- brighten contrasting panels
- dim borders
- retain v0.1.3 for those that prefer earlier style

## [0.1.3]

- update readme and screenshot

## [0.1.2]

- fix source control graph badge colors: brighten scmGraph.historyItemRefColor
- fix titlebar border, FG/BG in custom mode

## [0.1.1]

- fix manifest and pub WF

## [0.1.0]

- dimmer button BG so as to not compete visually with the project names in the Git sidebar
- fix manifest repo links
- retain v0.0.4 for those who prefer the earlier style

## [0.0.4]

- make badge BG transparent

## [0.0.3]

- fix minimap section header BG

## [0.0.2]

- swap button.FG/BG

## [0.0.1]

- Initial release
