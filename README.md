# Royalish Theme

Royalish color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-royalish_code.png](./images/sjsepan-royalish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-royalish_codium.png](./images/sjsepan-royalish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-royalish_codedev.png](./images/sjsepan-royalish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-royalish_ads.png](./images/sjsepan-royalish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-royalish_theia.png](./images/sjsepan-royalish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-royalish_positron.png](./images/sjsepan-royalish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/10/2025
